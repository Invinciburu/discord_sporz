from random import randint, shuffle

from board_computer.game import Game
from board_computer.players.astronaut import Astronaut
from board_computer.players.computer_scientist import ComputerScientist
from board_computer.players.doctor import Doctor
from board_computer.players.fanatic import Fanatic
from board_computer.players.analysts.geneticist import Geneticist
from board_computer.players.hackers.hacker import Hacker
from board_computer.players.hackers.hacker_apprentice import HackerApprentice
from board_computer.players.painter import Painter
from board_computer.players.patient_zero import PatientZero
from board_computer.players.politician import Politician
from board_computer.players.analysts.psychologist import Psychologist
from board_computer.players.analysts.spy import Spy
from board_computer.players.variables import FANATIC, PAINTER, ASTRONAUT, DOCTOR, PATIENT_ZERO, COMPUTER_SCIENTIST, \
    PSYCHOLOGIST, GENETICIST, POLITICIAN, HACKER, HACKER_APPRENTICE, SPY, HOST_GENOME, RESISTANT_GENOME, NORMAL_GENOME
from discord_bot.speaker import announcement

ROLE_CLASS_MAPPING = {
    ASTRONAUT: Astronaut,
    DOCTOR: Doctor,
    PATIENT_ZERO: PatientZero,
    COMPUTER_SCIENTIST: ComputerScientist,
    PSYCHOLOGIST: Psychologist,
    GENETICIST: Geneticist,
    POLITICIAN: Politician,
    HACKER: Hacker,
    HACKER_APPRENTICE: HackerApprentice,
    SPY: Spy,
    PAINTER: Painter,
    FANATIC: Fanatic,
}


class GameFactory(object):
    def __init__(self):
        self.players = None
        self.state = {
            'player_names': None,
            'resistant_genome_count': None,
            'host_genome_count': None,
            'patient_zero_count': None,
            'doctor_count': None,
            'has_computer_scientist': None,
            'has_psychologist': None,
            'has_geneticist': None,
            'has_politician': None,
            'has_hacker': None,
            'has_hacker_apprentice': None,
            'has_spy': None,
            'has_painter': None,
            'has_fanatic': None,
        }
        self.announce_next_attribute_to_ask()

    @property
    def next_attribute_to_ask(self):
        for attribute, value in self.state.items():
            if value is None:
                if attribute.endswith('_count'):
                    expected_type = int
                elif attribute.startswith('has_'):
                    expected_type = bool
                else:
                    expected_type = list
                return attribute, expected_type
        return None, None

    def announce_next_attribute_to_ask(self):
        attribute_to_enter, expected_type = self.next_attribute_to_ask
        announcement(f'Please enter the {attribute_to_enter} ({expected_type})')

    def enter_next_input(self, players_input):
        attribute_to_enter, expected_type = self.next_attribute_to_ask
        if attribute_to_enter is not None:
            if isinstance(players_input, expected_type):
                self.state[attribute_to_enter] = players_input
                self.announce_next_attribute_to_ask()
            else:
                raise TypeError(f'{attribute_to_enter} should be of type {expected_type} but received {input}')
        else:
            raise Exception('Nothing to input!')

    def start_game(self):
        self._assert_state_is_ok()
        self._generate_players()
        return Game(self.players)

    def _assert_state_is_ok(self):
        assert self.next_attribute_to_ask == (None, None), 'There are still some inputs to enter!'
        players_given = len(self.state['player_names'])
        genomes_given, roles_given = self._count_genomes_and_roles()
        assert players_given >= genomes_given + self.state['patient_zero_count'] + self.state['doctor_count'], \
            'There are more genomes than players! Keep in mind that doctors and patients zero have a fixed genome.'
        assert players_given >= roles_given, 'There are more players than players!'

    def _count_genomes_and_roles(self):
        roles_given = 0
        genomes_given = 0
        for attribute, value in self.state.items():
            if 'genome' in attribute:
                genomes_given += value
            elif attribute.endswith('_count'):
                roles_given += value
            elif attribute.startswith('has_') and value:
                roles_given += 1
        return genomes_given, roles_given

    def _generate_players(self):
        roles_to_attribute = self._roles_to_attribute()
        shuffle(roles_to_attribute)
        self.players = [
            ROLE_CLASS_MAPPING[roles_to_attribute[i]](self.state['player_names'][i])
            for i in range(len(roles_to_attribute))
        ]
        self._set_genomes()

    def _roles_to_attribute(self):
        roles = []
        for role in [DOCTOR, PATIENT_ZERO]:
            for _ in range(self.state[f'{role}_count']):
                roles.append(role)
        for attribute, value in self.state.items():
            if attribute.startswith('has_') and value:
                roles.append(attribute[4:])
        for _ in range(len(self.state['player_names']) - len(roles)):
            roles.append(ASTRONAUT)
        return roles

    def _players_with_normal_genomes(self):
        return [player for player in self.players if player.genome == NORMAL_GENOME]

    def _set_genomes(self):
        normal_players = [player for player in self.players if not(isinstance(player, (Doctor, PatientZero)))]
        for genome in [HOST_GENOME, RESISTANT_GENOME]:
            for _ in range(self.state[f'{genome}_genome_count']):
                i = randint(0, len(normal_players) - 1)
                normal_players[i].genome = genome
                normal_players.pop(i)
