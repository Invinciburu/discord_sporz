from board_computer.day import Day
from board_computer.night import Night
from board_computer.players.analysts.geneticist import Geneticist
from board_computer.players.analysts.psychologist import Psychologist
from board_computer.players.analysts.spy import Spy
from board_computer.players.doctor import Doctor
from board_computer.players.hackers.hacker import Hacker
from board_computer.players.hackers.hacker_apprentice import HackerApprentice
from board_computer.players.variables import MUTATE, KILL, HEAL
from discord_bot.speaker import announcement, player_message


class PlayerEventManager(object):
    def __init__(self, game):
        self.game = game

    def apply_event(self, event, player_name, **kwargs):
        assert not event.startswith('_'), 'Cheating is not permitted!'
        getattr(self, event)(player_name, **kwargs)

    def vote_for_chief_election(self, player_name, **kwargs):
        vote = kwargs['vote']
        self._assert_that_there_is_currently_a_chief_election()
        voting_player = self.game.get_living_player_by_name(player_name)
        assert vote in [player.name for player in self.game.living_players], \
            'The name on the vote is not a current player!'
        for k, v in voting_player.chief_votes.items():
            if v is None:
                voting_player.chief_votes[k] = vote
                announcement('Vote casted!')
                return
        raise Exception('Player already voted. Vote cannot be changed for chief election.')

    def _assert_that_there_is_currently_a_chief_election(self):
        day = self.game.current_day_or_night
        if isinstance(day, Day) and not day.chief_election_has_not_started and not day.chief_election_is_over():
            return
        raise AssertionError('There is currently no chief election.')

    def vote_for_death_election(self, player_name, **kwargs):
        vote = kwargs['vote']
        self._assert_that_there_is_currently_a_death_election()
        voting_player = self.game.get_living_player_by_name(player_name)
        assert vote in [None] + [player.name for player in self.game.living_players], 'Incorrect vote!'
        if len(voting_player.death_votes) - 1 == self.game.date:
            player_message(player_name, 'Player already voted, updating vote...')
            voting_player.death_votes[self.game.date] = vote
        voting_player.death_votes.append(vote)
        announcement('Vote casted!')

    def chief_choice(self, player_name, **kwargs):
        choice = kwargs['choice']
        self._assert_that_there_is_currently_a_death_election()
        assert player_name == self.game.chief, 'You\'re not the chief!'
        assert choice in self.game.current_day_or_night.death_designees, 'Please choose amongst the results!'
        self.game.current_day_or_night.chief_choice = choice
        announcement('The chief has chosen.')

    def _assert_that_there_is_currently_a_death_election(self):
        day = self.game.current_day_or_night
        if isinstance(day, Day) and not day.death_election_has_not_started and day.death_election_has_not_ended:
            return
        raise AssertionError('There is currently no death election.')

    def vote_for_infected_election(self, player_name, **kwargs):
        vote = kwargs['vote']
        night = self.game.current_day_or_night
        voting_player = self.game.get_living_player_by_name(player_name)
        assert voting_player.is_infected, 'You\'re not infected, you can\'t vote!'
        self._assert_that_there_is_currently_an_infected_election(night)
        if not night.infected_action:
            assert vote in [MUTATE, KILL], 'You can only vote for "mutate" or "kill".'
        else:
            assert vote in [player.name for player in night.players], \
                'The name on the vote is not a current player!'
        if voting_player.infected_choice is not None:
            player_message(player_name, 'Player already voted, updating vote...')
        voting_player.infected_choice = vote
        announcement('Vote casted!')

    @staticmethod
    def _assert_that_there_is_currently_an_infected_election(night):
        if isinstance(night, Night) and night.infected_have_started_playing and not night.infected_have_played:
            return
        raise AssertionError('There is currently no infected election.')

    def vote_for_doctors_election(self, player_name, **kwargs):
        vote = kwargs['vote']
        night = self.game.current_day_or_night
        voting_player = self.game.get_living_player_by_name(player_name)
        assert isinstance(voting_player, Doctor), 'You\'re not a doctor, you can\'t vote!'
        self._assert_that_there_is_currently_a_doctors_election(night)
        if not night.doctors_action:
            assert vote in [HEAL, KILL], 'You can only vote for "heal" or "kill".'
        else:
            assert vote in [player.name for player in night.players], \
                'The name on the vote is not a current player!'
        if voting_player.doctor_choice is not None:
            player_message(player_name, 'Player already voted, updating vote...')
        voting_player.doctor_choice = vote
        announcement('Vote casted!')

    @staticmethod
    def _assert_that_there_is_currently_a_doctors_election(night):
        if isinstance(night, Night) and night.doctors_have_started_playing and not night.doctors_have_played:
            return
        raise AssertionError('There is currently no doctor election.')

    def _player_night(self, player_name, role_class, role, choice_is_a_living_player=False, **kwargs):
        choice = kwargs['choice']
        player = self.game.get_living_player_by_name(player_name)
        assert isinstance(player, role_class), f'You\'re not a {role}!'
        assert isinstance(self.game.current_day_or_night, Night), 'It\'s not the night!'
        assert player.nightly_routine_has_started, 'Please wait for the infected and the doctors to play.'
        assert not player.nightly_routine_is_over, 'There is nothing more you can do tonight.'
        if choice_is_a_living_player:
            assert player in self.game.current_day_or_night.players, 'You can\'t choose this player, he\'s dead.'
        player.input_choice(choice)
        player.role_nightly_routine(self.game.current_day_or_night.players)

    def hacker_night(self, player_name, **kwargs):
        self._player_night(player_name, Hacker, 'hacker', **kwargs)

    def hacker_apprentice_night(self, player_name, **kwargs):
        self._player_night(player_name, HackerApprentice, 'hacker apprentice', choice_is_a_living_player=True, **kwargs)

    def psychologist_night(self, player_name, **kwargs):
        self._player_night(player_name, Psychologist, 'psychologist', choice_is_a_living_player=True, **kwargs)

    def geneticist_night(self, player_name, **kwargs):
        self._player_night(player_name, Geneticist, 'geneticist', choice_is_a_living_player=True, **kwargs)

    def spy_night(self, player_name, **kwargs):
        self._player_night(player_name, Spy, 'spy', choice_is_a_living_player=True, **kwargs)

