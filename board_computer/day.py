from random import shuffle

from board_computer.election import election_results
from discord_bot.speaker import announcement


class Day(object):
    def __init__(self, game):
        self.game = game
        self.date = game.date
        self.chief_election_count = 0
        self.chief_election_has_not_started = True
        self.death_election_has_not_started = True
        self.death_election_has_not_ended = True
        self.death_designees = None
        self.asked_chief_to_choose = None
        self.chief_choice = None

    def trigger_next_step(self):
        if self.game.chief is None:
            self.chief_election_next_step()
            return
        if self.death_election_has_not_ended:
            self.death_election_next_step()
            return
        self.game.trigger_night()

    def chief_election_next_step(self):
        if self.chief_election_has_not_started:
            self.trigger_chief_election()
        if self.chief_election_is_over():
            self.end_chief_election()

    def trigger_chief_election(self):
        self.chief_election_count += 1
        for player in self.game.living_players:
            player.chief_votes[self._chief_election_name()] = None
        self.chief_election_has_not_started = False
        announcement('Let\'s start the election of a chief!')

    def _chief_election_name(self):
        return f'chief_election_day_{self.date}_number_{self.chief_election_count}'

    def chief_election_results(self):
        votes = [player.chief_votes[self._chief_election_name()] for player in self.game.living_players]
        return election_results(votes)

    def chief_election_is_over(self):
        return not any(player.chief_votes[self._chief_election_name()] is None for player in self.game.living_players)

    def end_chief_election(self):
        designees, results = self.chief_election_results()
        announcement(f'The election results are: {results}!')
        if len(designees) == 1:
            new_chief = designees[0]
        else:
            announcement(f'There is an equality between those astronauts: {", ".join(designees)}')
            announcement('Therefore, I, the board computer will choose my favorite.')
            shuffle(designees)
            new_chief = designees[0]
        self.game.chief = new_chief
        announcement(f'The new elected chief is {new_chief}!')
        self.chief_election_has_not_started = True
        self.game.trigger_next_step()

    def board_computer_choose_new_chief(self, results):
        pass

    def death_election_next_step(self):
        if self.death_election_has_not_started:
            self.trigger_death_election()
        if self.death_election_is_over():
            self.end_death_election()

    def trigger_death_election(self):
        self.death_election_has_not_started = False
        announcement('Let\'s decide if we kill someone !')

    def death_election_is_over(self):
        return all(len(player.death_votes) - 1 == self.date for player in self.game.living_players)

    def death_election_result(self):
        votes = [player.death_votes[self.date] for player in self.game.living_players]
        return election_results(votes)

    def end_death_election(self):
        designees, results = self.death_election_result()
        announcement(f'The election results are: {results}!')
        if len(designees) == 1:
            guilty_astronaut = designees[0]
        else:
            guilty_astronaut = self.ask_chief_to_choose(designees)
            if guilty_astronaut is None:
                return
        if guilty_astronaut is None:
            announcement('There was a majority of blank votes, so, no one dies today.')
        else:
            announcement(f'{guilty_astronaut} was sentenced to death.')
            self.game.get_living_player_by_name(guilty_astronaut).die()
        self.death_election_has_not_ended = False
        self.game.trigger_next_step()

    def ask_chief_to_choose(self, designees):
        real_designees = [player for player in designees if player is not None]
        if len(real_designees) == 1:
            return real_designees[0]
        if not self.asked_chief_to_choose:
            announcement(f'There is an equality between those astronauts: {", ".join(real_designees)}')
            announcement('Therefore, the chief needs to choose who will be sentenced to death.')
            self.death_designees = real_designees
            return None
        return self.chief_choice
