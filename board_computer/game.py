from copy import copy

from board_computer import utils
from board_computer.day import Day
from board_computer.player_event_manager import PlayerEventManager
from board_computer.night import Night
from discord_bot.speaker import announcement, player_message


class Game(object):
    def __init__(self, players):
        self.players = players
        self.living_players = copy(players)
        self.chief = None
        self.date = 0
        self.current_day_or_night = None
        self.day_and_nights = []
        self.player_event_manager = PlayerEventManager(self)
        self.game_over = False
        self.print_game()

    def start(self):
        announcement(f'The night falls on the ship and all the astronauts go to sleep.')
        self._trigger(Night)

    def _trigger(self, day_or_night_class):
        day_or_night = day_or_night_class(self)
        self.current_day_or_night = day_or_night
        self.day_and_nights.append(day_or_night)
        self.trigger_next_step()

    def trigger_day(self):
        announcement(f'Day {self.date} since the beginning of the infection.')
        self._trigger(Day)

    def trigger_night(self):
        announcement(f'Night {self.date} since the beginning of the infection.')
        self._trigger(Night)

    def trigger_next_day(self):
        self.date += 1
        self._trigger(Day)

    def print_game(self):  # TODO debug only
        for player in self.players:
            print(player.name, player.role, player.genome, player.is_infected)

    def player_event_input(self, event, player_name, **kwargs):
        if self.game_over:
            player_message(player_name, 'Game is over!')
            return
        try:
            self.player_event_manager.apply_event(event, player_name, **kwargs)
        except Exception as e:
            player_message(player_name, f'Player event failure: {e}')
        self.trigger_next_step()

    def trigger_next_step(self):
        if self.game_over:
            return
        self.trigger_automatic_steps()
        if self.game_over:
            return
        self.current_day_or_night.trigger_next_step()

    def trigger_automatic_steps(self):
        freshly_killed_players = [player for player in self.living_players if not player.is_alive]
        for player in freshly_killed_players:
            self.living_players.remove(player)
        if isinstance(self.current_day_or_night, Day):
            unrevealed_dead = [player for player in self.players if not player.is_alive and not player.is_revealed]
            for player in unrevealed_dead:
                player.reveal()
            self.check_win_condition()
            if self.game_over:
                return
        if self.chief is not None:
            if not self.get_player_by_name(self.chief).is_alive:
                announcement('The chief of the shipped just died.')
                self.chief = None

    def check_win_condition(self):
        if all(player.is_infected for player in self.living_players):
            print('Game over! The infected won!')
            self.game_over = True
        if not any(player.is_infected for player in self.living_players):
            print('Game over! The healthy won!')
            self.game_over = True

    def get_player_by_name(self, player_name):
        return utils.get_player_by_name(player_name, self.players)

    def get_living_player_by_name(self, player_name):
        player = self.get_player_by_name(player_name)
        if player.is_alive:
            return player
        else:
            raise Exception('You\'re dead!')
