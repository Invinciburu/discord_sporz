from collections import Counter


def election_results(votes: list):
    results = Counter(votes)
    winners = [k for k, v in results.items() if v == max(results.values())]
    return winners, dict(results)
