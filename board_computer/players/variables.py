#  GENOMES
HOST_GENOME = 'host'
RESISTANT_GENOME = 'resistant'
NORMAL_GENOME = 'normal'

#  ROLES
ASTRONAUT = 'astronaut'
DOCTOR = 'doctor'
PATIENT_ZERO = 'patient_zero'
COMPUTER_SCIENTIST = 'computer_scientist'
PSYCHOLOGIST = 'psychologist'
GENETICIST = 'geneticist'
POLITICIAN = 'politician'
HACKER = 'hacker'
HACKER_APPRENTICE = 'hacker_apprentice'
SPY = 'spy'
PAINTER = 'painter'
FANATIC = 'fanatic'

#  ACTIONS
KILL = 'kill'
MUTATE = 'mutate'
HEAL = 'heal'

ASTRONAUT_NIGHTS = [
    'Someone knocks on your door. You open and you discover a pile of flesh that jumps on you! '
    'You wake up all sweaty. Luckily, it was just a nightmare. You go back to sleep.',
    'You can\'t stop thinking about the other astronauts. Who can you really trust aboard this ship? You think '
    'about it for two hours before finally falling asleep.',
    'Before going to bed, you remember a bad joke that Joe, your american friend, told you before you left for '
    'the expedition :\n'
    '- Astronaut 1: "I can\'t find any milk for my coffee"\n'
    '- Astronaut 2: "In space no one can. Here, use cream"\n'
    'You smile for a bit before falling on your bed, exhausted.',
    'In the middle of the night, you wake up thirsty. You grab the bottle next to you and you start looking '
    'at the ceiling. Suddenly, a black tentacle comes out of your air vent! You close your eyes in disbelief '
    'but the moment that you open them back, the tentacle has disappeared. Was it real? No, of course it '
    'wasn\'t. You have to remain cool-headed if you want everything to go well. You decide to go back to '
    'sleep to be at your best tomorrow.',
    'At one point during the night, you woke yourself up with your own snore. Then, you went back to sleep.'
]
