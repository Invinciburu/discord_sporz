from board_computer.players.analysts.geneticist import Geneticist
from board_computer.players.analysts.psychologist import Psychologist
from board_computer.players.analysts.spy import Spy
from board_computer.players.hackers.hacker import AbstractHacker
from board_computer.players.variables import HACKER_APPRENTICE, NORMAL_GENOME
from board_computer.utils import infection_string, get_player_by_name


class HackerApprentice(AbstractHacker):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=HACKER_APPRENTICE)

    def hack_analyst(self):
        self.tell(f'The {self.player_to_hack.role} analyzed someone tonight.')
        player_analyzed = get_player_by_name(self.player_to_hack.astronaut_to_analyze, self.tonight_players)
        if isinstance(self.player_to_hack, Psychologist):
            self.tell(f'Its conclusions were that this astronaut is {infection_string(player_analyzed.is_infected)}.')
        elif isinstance(self.player_to_hack, Geneticist):
            self.tell(
                f'Its conclusions were that this astronaut has a {infection_string(player_analyzed.genome)} genome.'
            )
        elif isinstance(self.player_to_hack, Spy):
            self.tell(
                f'Its conclusions were that this astronaut was:\n'
                f'{Spy.analysis(player_analyzed, self.tonight_players)}'
            )
