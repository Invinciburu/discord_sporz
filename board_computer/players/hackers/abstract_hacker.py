from board_computer.players.analysts.geneticist import Geneticist
from board_computer.players.analysts.psychologist import Psychologist
from board_computer.players.analysts.spy import Spy
from board_computer.players.astronaut import Astronaut
from board_computer.players.computer_scientist import ComputerScientist
from board_computer.players.variables import NORMAL_GENOME, COMPUTER_SCIENTIST
from board_computer.utils import get_player_by_role

HACKABLE_ROLES = [ComputerScientist, Psychologist, Geneticist, Spy]


class AbstractHacker(Astronaut):
    def __init__(self, name, role, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=role)
        self.last_night_choice = None
        self.this_night_choice = None
        self.living_hackable_players = None
        self.tonight_players = None
        self.player_to_hack = None
        self.showed_waiting_message = None

    def input_choice(self, choice):
        assert self.this_night_choice is None, f'You\'re already hacking the {self.this_night_choice}\'s computer!'
        hackable_roles = [player.role for player in self._hackable_players()]
        assert choice in hackable_roles, f'You can\'t. Your only options are: {", ".join(hackable_roles)}'
        self.this_night_choice = choice

    def _hackable_players(self):
        return [
            player for player in self.tonight_players
            if any(isinstance(player, role) for role in HACKABLE_ROLES) and player.role != self.last_night_choice
        ]

    def role_nightly_routine(self, players):
        self.tonight_players = players
        if not self.nightly_routine_has_started:
            self.nightly_routine_has_started = True
            self._ask_which_computer_to_hack()
            return
        if not self.this_night_choice:
            return
        self.player_to_hack = get_player_by_role(self.this_night_choice, self.tonight_players)
        if self.player_to_hack.is_paralyzed or self.player_to_hack.is_infected \
                or not self.player_to_hack.is_alive:
            self.tell(f'It appears that the {self.player_to_hack.role_display_name} did nothing special tonight.')
            self.end_nightly_routine()
        if not self.player_to_hack.nightly_routine_is_over:
            if not self.showed_waiting_message:
                self.tell(
                    f'You start hacking the {self.player_to_hack.role_display_name}\'s computer. '
                    f'It may take a while...'
                )
                self.showed_waiting_message = True
            return
        self._hack()
        self.end_nightly_routine()

    def _ask_which_computer_to_hack(self):
        hackable_players = self._hackable_players()
        if hackable_players:
            self.tell('Choose which computer you want to hack into:')
            for player in hackable_players:
                self.tell(f'- The {player.role_display_name}\'s computer?')
        else:
            self.tell('There is no computer that you can hack into tonight.')
            self.end_nightly_routine()

    def _hack(self):
        if self.this_night_choice == COMPUTER_SCIENTIST:
            count = ComputerScientist.infected_count(self.tonight_players)
            self.tell(f'The computer scientist saw that there is currently {count} infected on board.')
        else:
            self.hack_analyst()

    def night_reset(self):
        super().night_reset()
        self.last_night_choice = self.this_night_choice
        self.this_night_choice = None
        self.living_hackable_players = None
        self.tonight_players = None
        self.player_to_hack = None
        self.showed_waiting_message = None

    def hack_analyst(self):
        raise Exception('AbstractHacker is an abstract class.')
