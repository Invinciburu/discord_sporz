from board_computer.players.hackers.abstract_hacker import AbstractHacker
from board_computer.players.variables import HACKER, NORMAL_GENOME


class Hacker(AbstractHacker):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=HACKER)

    def hack_analyst(self):
        self.tell(f'The {self.player_to_hack.role} analyzed {self.player_to_hack.astronaut_to_analyze} tonight.')
