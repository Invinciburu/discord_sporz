from board_computer.players.astronaut import Astronaut
from board_computer.players.variables import COMPUTER_SCIENTIST, NORMAL_GENOME


class ComputerScientist(Astronaut):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=COMPUTER_SCIENTIST)

    def role_nightly_routine(self, players):
        self.tell(f'Computer scientist, there are currently {self.infected_count(players)} infected on board.')
        self.nightly_routine_is_over = True

    @staticmethod
    def infected_count(players):
        return len([player for player in players if player.is_infected])
