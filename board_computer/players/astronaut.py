import random

from board_computer.players.variables import HOST_GENOME, NORMAL_GENOME, RESISTANT_GENOME, ASTRONAUT_NIGHTS
from board_computer.utils import infection_string
from discord_bot.speaker import player_message, announcement


class Astronaut(object):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False, role='astronaut'):
        self.is_alive = True
        self.is_revealed = False
        self.name = name
        self.genome = genome
        self.is_infected = is_infected
        self.was_infected_tonight = False
        self.was_healed_tonight = False
        self.role = role
        self.role_display_name = role.replace("_", " ")
        self.is_chief = False
        self.death_votes = [None]
        self.chief_votes = {}
        self.nightly_routine_has_started = False
        self.nightly_routine_is_over = False
        self.is_paralyzed = False
        self.infected_choice = None
        self.nicknames = ['astronaut']
        self.night_history = []

    def tell(self, message):
        player_message(self.name, message)

    def nightly_routine(self, players):
        if self.nightly_routine_is_over:
            return
        if self.is_alive and not self.is_paralyzed and not self.is_infected:
            self.role_nightly_routine(players)
        else:
            self.nightly_routine_has_started = True
            self.tell(f'{self.name}, you can\'t play tonight.')
            self.nightly_routine_is_over = True

    def role_nightly_routine(self, players):
        self.end_nightly_routine()

    def end_nightly_routine(self):
        if self.nightly_routine_has_started:
            self.tell(f'Now, go back to sleep, {random.choice(self.nicknames)}.')
        else:
            self.tell(random.choice(ASTRONAUT_NIGHTS))
        self.nightly_routine_is_over = True

    def night_reset(self):
        self.is_paralyzed = False
        self.was_infected_tonight = False
        self.was_healed_tonight = False
        self.nightly_routine_is_over = False

    def get_paralyzed(self):
        self.tell(f'{self.name}, the infected are paralyzing you. For the rest of the night, you won\'t be able to play.')
        self.is_paralyzed = True

    def get_infected(self):
        self.was_infected_tonight = True
        self.tell(f'{self.name}, the infected are trying to mutate you.')
        if self.genome == RESISTANT_GENOME:
            self.tell('However, because of your resistant genome, you remain healthy.')
        else:
            self.tell('The mutation worked! You are now infected!')
            self.is_infected = True

    def heal(self):
        self.was_healed_tonight = True
        self.tell(f'{self.name}, the doctors are trying to heal you.')
        if not self.is_alive:
            self.tell('However, you\'re dead so it won\'t work.')
        elif not self.is_infected:
            self.tell('But because you\'re not infected, nothing happens.')
        elif self.genome == HOST_GENOME:
            self.tell('However, because of your host genome, you remain infected.')
        elif self.genome == NORMAL_GENOME:
            self.tell('The cure worked! You are now healthy again!')
            self.is_infected = False
        else:
            raise ValueError('A resistant cannot be infected.')

    def die(self):
        self.is_alive = False

    def reveal(self):
        self.is_revealed = True
        infected_state = infection_string(self.is_infected)
        announcement(f'{self.name} died.')
        announcement(f'{self.name} was a {self.role}, had a {self.genome} genome and was {infected_state}.')
