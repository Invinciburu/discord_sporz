from board_computer.players.astronaut import Astronaut
from board_computer.players.variables import HOST_GENOME, PATIENT_ZERO


class PatientZero(Astronaut):
    def __init__(self, name):
        super().__init__(name, genome=HOST_GENOME, is_infected=True, role=PATIENT_ZERO)
