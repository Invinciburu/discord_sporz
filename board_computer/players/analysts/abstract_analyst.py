from board_computer.players.astronaut import Astronaut
from board_computer.players.variables import NORMAL_GENOME
from board_computer.utils import get_player_by_name


class AbstractAnalyst(Astronaut):
    def __init__(self, name, role, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=role)
        self.astronaut_to_analyze = None

    def input_choice(self, choice):
        assert self.astronaut_to_analyze is None, 'Unexpected error'
        self.astronaut_to_analyze = choice

    def role_nightly_routine(self, players):
        if not self.nightly_routine_has_started:
            self.nightly_routine_has_started = True
            self.tell('Tell me who you want to analyze tonight.')
            return
        if self.astronaut_to_analyze:
            player_to_analyze = get_player_by_name(self.astronaut_to_analyze, players)
            self.analyze_astronaut(player_to_analyze, players)

    def analyze_astronaut(self, player_to_analyze, players):
        raise Exception('AbstractAnalyst is an abstract class.')

    def night_reset(self):
        super().night_reset()
        self.astronaut_to_analyze = None
