from board_computer.players.analysts.abstract_analyst import AbstractAnalyst
from board_computer.players.variables import PSYCHOLOGIST, NORMAL_GENOME
from board_computer.utils import infection_string


class Psychologist(AbstractAnalyst):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=PSYCHOLOGIST)
        self.nicknames = [
            'Freud',
            'Rorschach',
            'Pavlov',
            'Carl Jung',
            'Zimbardo',
            'Professor X',
            'Skinner',
            'and don\'t dream of fucking your mother, OEdipus',
            '',
        ]

    def analyze_astronaut(self, player_to_analyze, _):
        infected_state = infection_string(player_to_analyze.is_infected)
        self.tell(f'{self.astronaut_to_analyze} is {infected_state}.')
        self.end_nightly_routine()
