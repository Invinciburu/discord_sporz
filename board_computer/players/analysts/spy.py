from board_computer.players.analysts.abstract_analyst import AbstractAnalyst
from board_computer.players.analysts.psychologist import Psychologist
from board_computer.players.variables import SPY, NORMAL_GENOME
from board_computer.utils import get_player_by_role


class Spy(AbstractAnalyst):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=SPY)
        self.already_waiting_for_psychologist = False
        self.nicknames = [
            'James Bond',
            '007',
            'Gadget',
            'Mata Hari',
            'Powers',
            'Solid Snake',
            'Big Boss',
            'Sam Fisher',
            'Agent 47',
        ]

    def analyze_astronaut(self, player_to_analyze, players):
        psychologist = get_player_by_role(Psychologist, players)
        if psychologist and psychologist.player_to_analyze is None:
            if self.already_waiting_for_psychologist:
                self.tell('Waiting for the psychologist to play...')
            return
        self.tell(f'Tonight, {player_to_analyze.name} was:\n{self.analysis(player_to_analyze, players)}')
        self.end_nightly_routine()

    @staticmethod
    def analysis(player_to_analyze, players):
        analysis = f'- infected: {Spy.bool_to_string(player_to_analyze.was_infected_tonight)}\n' \
                   f'- paralyzed: {Spy.bool_to_string(player_to_analyze.is_paralyzed)}\n' \
                   f'- healed: {Spy.bool_to_string(player_to_analyze.was_healed_tonight)}\n'
        psychologist = get_player_by_role(Psychologist, players)
        if psychologist:
            was_inspected_by_psychologist = psychologist.player_to_analyze == player_to_analyze
            analysis += f'- inspected by the psychologist: {Spy.bool_to_string(was_inspected_by_psychologist)}\n'
        return analysis

    @staticmethod
    def bool_to_string(boolean):
        return 'YES' if boolean else 'NO'
