from board_computer.players.analysts.abstract_analyst import AbstractAnalyst
from board_computer.players.variables import GENETICIST, NORMAL_GENOME


class Geneticist(AbstractAnalyst):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=GENETICIST)
        self.nicknames = [
            'Darwin',
            'allele lover',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
        ]

    def analyze_astronaut(self, player_to_analyze, _):
        genome = player_to_analyze.genome
        self.tell(f'{self.astronaut_to_analyze} has a {genome} genome.')
        self.end_nightly_routine()
