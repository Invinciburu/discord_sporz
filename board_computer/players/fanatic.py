from board_computer.players.astronaut import Astronaut
from board_computer.players.variables import FANATIC, NORMAL_GENOME


class Fanatic(Astronaut):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=FANATIC)

    def role_nightly_routine(self, players):
        pass  # TODO
