from board_computer.players.astronaut import Astronaut
from board_computer.players.variables import PAINTER, NORMAL_GENOME


class Painter(Astronaut):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=PAINTER)

    def role_nightly_routine(self, players):
        pass  # TODO
