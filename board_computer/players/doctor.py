from board_computer.players.astronaut import Astronaut
from board_computer.players.variables import DOCTOR, NORMAL_GENOME


class Doctor(Astronaut):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=DOCTOR)
        self.doctor_choice = None

    def night_reset(self):
        super().night_reset()
        self.doctor_choice = None
