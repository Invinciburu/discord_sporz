from board_computer.players.astronaut import Astronaut
from board_computer.players.variables import POLITICIAN, NORMAL_GENOME


class Politician(Astronaut):
    def __init__(self, name, genome=NORMAL_GENOME, is_infected=False):
        super().__init__(name, genome=genome, is_infected=is_infected, role=POLITICIAN)

    def role_nightly_routine(self, players):
        pass  # TODO
