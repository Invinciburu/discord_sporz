def get_player_by_name(player_name, players):
    for player in players:
        if player.name == player_name:
            return player
    raise Exception('player_name is unknown')


def get_player_by_role(role, players):
    for player in players:
        if isinstance(role, classmethod) and isinstance(player, role):
            return player
        if isinstance(role, str) and player.role == role:
            return player
    return None


def infection_string(is_infected):
    return 'infected' if is_infected else 'healthy'
