from board_computer.players.doctor import Doctor
from board_computer.players.variables import MUTATE, KILL, HEAL
from discord_bot import speaker


class Night(object):
    def __init__(self, game):
        self.game = game
        self.players = game.living_players
        self.date = game.date
        self.infected_have_started_playing = False
        self.infected_action = None
        self.infected_victim = None
        self.infected_paralyzed_victim = None
        self.infected_have_played = False
        self.night_doctors = None
        self.doctors_have_started_playing = False
        self.doctors_action = None
        self.doctors_patients = None
        self.doctors_have_played = False
        for player in self.players:
            player.nightly_routine_is_over = False

    def trigger_next_step(self):
        if not self.infected_have_played:
            self.infected_next_step()
            return
        if not self.doctors_have_played:
            self.doctors_next_step()
            return
        for player in self.players:
            player.nightly_routine(self.players)
        if all(player.nightly_routine_is_over for player in self.players):
            self.reset_players_state()
            self.game.trigger_next_day()

    def infected_announcement(self, message):
        infected_names = [player.name for player in self.players if player.is_infected]
        speaker.infected_announcement(self.date, infected_names, message)

    def infected_next_step(self):
        if not self.infected_have_started_playing:
            self.infected_announcement('Infected, please choose to either kill or mutate someone.')
            self.infected_have_started_playing = True
            return
        infected_choices = [player.infected_choice for player in self.players if player.is_infected]
        if None in infected_choices:
            return
        if len(set(infected_choices)) != 1:
            self.infected_announcement('Everyone does not agree, please vote again.')
            self.reset_infected_choice()
            return
        self.apply_infected_choice(infected_choices[0])

    def reset_infected_choice(self):
        for player in self.players:
            if player.is_infected:
                player.infected_choice = None

    def apply_infected_choice(self, infected_choice):
        if not self.infected_action:
            self.infected_action = infected_choice
            self.infected_announcement(f'Now, choose who you want to {self.infected_action}.')
        elif not self.infected_victim:
            self.infected_victim = infected_choice
            self.infected_announcement(f'Now, choose who you want to paralyze.')
        elif not self.infected_paralyzed_victim:
            self.infected_paralyzed_victim = infected_choice
        self.reset_infected_choice()
        if self.infected_action and self.infected_victim and self.infected_paralyzed_victim:
            self.end_infected_turn()

    def end_infected_turn(self):
        self.infected_announcement(
            f'You have decided to {self.infected_action} {self.infected_victim} and paralyze '
            f'{self.infected_paralyzed_victim}.\n'
            f'Thank you, infected, now go back to sleep.'
        )
        if self.infected_action == MUTATE:
            self.game.get_living_player_by_name(self.infected_victim).get_infected()
        else:
            self.game.get_living_player_by_name(self.infected_victim).die()
        self.game.get_living_player_by_name(self.infected_paralyzed_victim).get_paralyzed()
        self.infected_have_played = True
        self.game.trigger_next_step()

    def doctors_announcement(self, message):
        infected_names = [player.name for player in self.players if player.is_infected]
        speaker.infected_announcement(self.date, infected_names, message)

    def doctors_next_step(self):
        self.night_doctors = [
            player for player in self.players
            if isinstance(player, Doctor) and not player.is_paralyzed and not player.is_infected
        ]
        if not self.doctors_have_started_playing:
            self.doctors_have_started_playing = True
            if len(self.night_doctors) == 0:
                self.doctors_have_played = True
                self.game.trigger_next_step()
            else:
                self.doctors_announcement(
                    f'Doctors, please choose to either heal {len(self.night_doctors)} people or kill 1 person.'
                )
                return
        doctors_choices = [player.doctor_choice for player in self.night_doctors]
        if None in doctors_choices:
            return
        self.apply_doctors_choice(doctors_choices)

    def apply_doctors_choice(self, doctors_choices):
        if not self.doctors_action or self.doctors_action == KILL:
            if len(set(doctors_choices)) != 1:
                self.doctors_announcement('Everyone does not agree, please vote again.')
                self.reset_doctors_choice()
                self.doctors_action = None
                return
            if not self.doctors_action:
                self.doctors_action = doctors_choices[0]
                self.reset_doctors_choice()
                if self.doctors_action == HEAL:
                    self.doctors_announcement('Now please choose who you want to heal (one person each).')
                    self.doctors_announcement('Make sure to heal different people because I won\'t check that.')
                else:
                    self.doctors_announcement('Now please choose who you want to kill.')
                    self.doctors_announcement('All doctors have to vote the same name or we\'ll start over.')
                return
        self.end_doctors_turn(doctors_choices)

    def reset_doctors_choice(self):
        for player in self.players:
            if isinstance(player, Doctor):
                player.doctor_choice = None

    def end_doctors_turn(self, doctors_choices):
        self.doctors_patients = doctors_choices
        if self.doctors_action == HEAL:
            self.doctors_announcement(f'You have decided to heal {" and ".join(doctors_choices)}.')
            for player in doctors_choices:
                self.game.get_living_player_by_name(player).heal()
        else:
            self.doctors_announcement(f'You have decided to kill {doctors_choices[0]}.')
            self.game.get_living_player_by_name(doctors_choices[0]).die()
        self.doctors_announcement('Thank you, doctors, now go back to sleep.')
        self.doctors_have_played = True
        self.game.trigger_next_step()

    def reset_players_state(self):
        for player in self.players:
            player.night_reset()
        self.reset_doctors_choice()
        self.reset_infected_choice()
