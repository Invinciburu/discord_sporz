from board_computer.game import Game
from board_computer.players.astronaut import Astronaut
from board_computer.players.computer_scientist import ComputerScientist
from board_computer.players.doctor import Doctor
from board_computer.players.analysts.geneticist import Geneticist
from board_computer.players.patient_zero import PatientZero
from board_computer.players.analysts.psychologist import Psychologist
from board_computer.players.variables import MUTATE, HEAL, RESISTANT_GENOME, HOST_GENOME


class TestNight:
    def start_sample_game(self):
        game = Game([
            Doctor('Alex'),
            Doctor('Amaury'),
            PatientZero('Zoé'),
            ComputerScientist('Viviane'),
            Psychologist('Éric'),
            Astronaut('Chloé', genome=RESISTANT_GENOME),
            Geneticist('Kiki', genome = HOST_GENOME),
        ])
        return game

    def start_sample_first_night(self):
        game = self.start_sample_game()
        game.start()
        night = game.current_day_or_night
        return game, night

    def test_infected_turn(self):
        game, night = self.start_sample_first_night()
        assert night.infected_have_started_playing
        game.player_event_input('vote_for_infected_election', player_name='Zoé', vote=MUTATE)
        game.player_event_input('vote_for_infected_election', player_name='Zoé', vote='Amaury')
        game.player_event_input('vote_for_infected_election', player_name='Zoé', vote='Alex')
        assert night.infected_action == MUTATE
        assert night.infected_victim == 'Amaury'
        assert night.infected_paralyzed_victim == 'Alex'
        assert night.infected_have_played

    def play_infected_turn(self, night):
        night.infected_action = MUTATE
        night.infected_victim = 'Viviane'
        night.infected_paralyzed_victim = 'Éric'
        night.infected_have_played = True
        night.end_infected_turn()

    def test_doctors_turn(self):
        game, night = self.start_sample_first_night()
        self.play_infected_turn(night)
        assert night.doctors_have_started_playing
        game.player_event_input('vote_for_doctors_election', player_name='Amaury', vote=HEAL)
        game.player_event_input('vote_for_doctors_election', player_name='Alex', vote=HEAL)
        game.player_event_input('vote_for_doctors_election', player_name='Amaury', vote='Viviane')
        game.player_event_input('vote_for_doctors_election', player_name='Alex', vote='Zoé')
        assert night.doctors_action == HEAL
        assert set(night.doctors_patients) == {'Viviane', 'Zoé'}
        assert night.doctors_have_played

