from board_computer.game import Game
from board_computer.game_factory import GameFactory


class TestFullGame:
    def test_full_game(self):
        game_factory = GameFactory()
        game_factory.enter_next_input(['Alex', 'Amaury', 'Zoé', 'Viviane', 'Éric', 'Chloé', 'Kiki'])
        game_factory.enter_next_input(1)
        game_factory.enter_next_input(1)
        game_factory.enter_next_input(1)
        game_factory.enter_next_input(2)
        game_factory.enter_next_input(True)
        game_factory.enter_next_input(True)
        game_factory.enter_next_input(True)
        game_factory.enter_next_input(False)
        game_factory.enter_next_input(False)
        game_factory.enter_next_input(False)
        game_factory.enter_next_input(False)
        game_factory.enter_next_input(False)
        game_factory.enter_next_input(False)
        assert isinstance(game_factory.start_game(), Game)