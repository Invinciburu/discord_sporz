from board_computer.game import Game
from board_computer.players.computer_scientist import ComputerScientist
from board_computer.players.doctor import Doctor
from board_computer.players.analysts.geneticist import Geneticist
from board_computer.players.hackers.hacker import Hacker
from board_computer.players.patient_zero import PatientZero
from board_computer.players.analysts.psychologist import Psychologist
from board_computer.players.variables import HOST_GENOME, RESISTANT_GENOME, MUTATE, HEAL


class TestFullGame:
    def test_full_game(self):
        game = Game([
            Doctor('Alex'),
            Doctor('Amaury'),
            PatientZero('Zoé'),
            ComputerScientist('Viviane'),
            Psychologist('Éric'),
            Hacker('Chloé', genome=RESISTANT_GENOME),
            Geneticist('Kiki', genome = HOST_GENOME),
        ])
        game.start()

        night = game.current_day_or_night
        assert night.infected_have_started_playing
        game.player_event_input('vote_for_infected_election', player_name='Zoé', vote=MUTATE)
        game.player_event_input('vote_for_infected_election', player_name='Zoé', vote='Chloé')
        game.player_event_input('vote_for_infected_election', player_name='Zoé', vote='Éric')
        assert night.infected_action == MUTATE
        assert night.infected_victim == 'Chloé'
        assert night.infected_paralyzed_victim == 'Éric'
        assert night.infected_have_played

        assert night.doctors_have_started_playing
        game.player_event_input('vote_for_doctors_election', player_name='Amaury', vote=HEAL)
        game.player_event_input('vote_for_doctors_election', player_name='Alex', vote=HEAL)
        game.player_event_input('vote_for_doctors_election', player_name='Amaury', vote='Viviane')
        game.player_event_input('vote_for_doctors_election', player_name='Alex', vote='Zoé')
        assert night.doctors_action == HEAL
        assert set(night.doctors_patients) == {'Viviane', 'Zoé'}
        assert night.doctors_have_played

        game.player_event_input('hacker_night', player_name='Chloé', choice='geneticist')
        game.player_event_input('geneticist_night', player_name='Kiki', choice='Chloé')

        # END OF NIGHT 0
