from board_computer.game import Game
from board_computer.players.astronaut import Astronaut
from board_computer.players.computer_scientist import ComputerScientist
from board_computer.players.doctor import Doctor
from board_computer.players.analysts.geneticist import Geneticist
from board_computer.players.patient_zero import PatientZero
from board_computer.players.analysts.psychologist import Psychologist
from board_computer.players.variables import RESISTANT_GENOME, HOST_GENOME


class TestDay:
    def start_sample_game(self):
        game = Game([
            Doctor('Alex'),
            Doctor('Amaury'),
            PatientZero('Zoé'),
            ComputerScientist('Viviane'),
            Psychologist('Éric'),
            Astronaut('Chloé', genome=RESISTANT_GENOME),
            Geneticist('Kiki', genome = HOST_GENOME),
        ])
        return game

    def start_sample_first_day(self, chief=None):
        game = self.start_sample_game()
        game.chief = chief
        game.start()
        game.trigger_next_day()
        day = game.current_day_or_night
        return game, day

    def test_day_start_properly(self):
        _, day = self.start_sample_first_day()
        assert day.date == 1
        assert day.chief_election_count == 1
        assert day.chief_election_has_not_started is False
        assert day.death_election_has_not_started is True
        assert day.death_election_has_not_ended is True

    def test_chief_election(self):
        game, day = self.start_sample_first_day()
        assert day.chief_election_has_not_started is False
        game.player_event_input('vote_for_chief_election', player_name='Alex', vote='Amaury')
        game.player_event_input('vote_for_chief_election', player_name='Amaury', vote='Alex')
        game.player_event_input('vote_for_chief_election', player_name='Zoé', vote='Amaury')
        game.player_event_input('vote_for_chief_election', player_name='Viviane', vote='Alex')
        game.player_event_input('vote_for_chief_election', player_name='Éric', vote='Amaury')
        game.player_event_input('vote_for_chief_election', player_name='Chloé', vote='Alex')
        assert day.chief_election_is_over() is False
        game.player_event_input('vote_for_chief_election', player_name='Kiki', vote='Amaury')
        assert day.chief_election_is_over() is True
        assert day.chief_election_results() == (['Amaury'], {'Amaury': 4, 'Alex': 3})
        assert game.chief == 'Amaury'

    def test_death_election(self):
        game, day = self.start_sample_first_day(chief='Amaury')
        assert day.death_election_has_not_started is False
        assert day.death_election_has_not_ended is True
        game.player_event_input('vote_for_death_election', player_name='Alex', vote='Amaury')
        game.player_event_input('vote_for_death_election', player_name='Amaury', vote='Alex')
        game.player_event_input('vote_for_death_election', player_name='Zoé', vote='Amaury')
        game.player_event_input('vote_for_death_election', player_name='Viviane', vote='Alex')
        game.player_event_input('vote_for_death_election', player_name='Éric', vote='Amaury')
        game.player_event_input('vote_for_death_election', player_name='Chloé', vote='Alex')
        assert day.death_election_has_not_ended is True
        game.player_event_input('vote_for_death_election', player_name='Kiki', vote='Amaury')
        assert day.death_election_has_not_ended is False
        assert game.get_player_by_name('Amaury').is_alive is False
        assert game.get_player_by_name('Amaury').is_revealed is True
