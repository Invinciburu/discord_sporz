import discord
from discord.ext import commands
from board_computer.game_factory import *

TOKEN = 'XXXXX'
GUILD = 'sporz'
game_started = False

client = discord.Client()

bot = commands.Bot(command_prefix='!')
game_factory = GameFactory()

@client.event
async def on_message(message):
    global game_started

    if message.content == '!init' and message.channel.name == 'général' and not game_started:
        members = [member.name for member in message.server.members]
        attribute, expected_type = game_factory.next_attribute_to_ask()
        game_factory.enter_next_input(members)
        while attribute is not None:
            attribute, expected_type = game_factory.next_attribute_to_ask()
            await client.send_message(message.channel, attribute)
            msg = await client.wait_for_message(check=None)
            if msg:
                await client.send_message(message.channel, msg.content)
                input = expected_type(msg.content)
                if expected_type == int:
                    game_factory.enter_next_input(int(input))
                elif expected_type == bool:
                    game_factory.enter_next_input(bool(input))
        await client.send_message(message.channel, "bye")
        game_started = True

    if message.content == "!vote" and game_started:
        await client.send_message(message.channel, "ok")

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


client.run(TOKEN)