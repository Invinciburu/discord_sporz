def _print(prefix, message):
    print(f'{prefix: <50} - {message}')


def announcement(message):
    prefix = 'Speaking in #general'
    _print(prefix, message)


def player_message(player_name, message):
    prefix = f'Speaking to {player_name}'
    _print(prefix, message)


def infected_announcement(date, player_names, message):
    prefix = f'Speaking in #infected_night_{date} ({", ".join(player_names)})'
    _print(prefix, message)


def doctors_announcement(date, player_names, message):
    prefix = f'Speaking in #doctors_night_{date} ({", ".join(player_names)})'
    _print(prefix, message)
